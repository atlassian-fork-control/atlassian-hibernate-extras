package com.atlassian.hibernate.extras;

public interface ExportProgress
{
    public void setStatus(String status);
    public int increment();
    public void setTotal(int total);
    public void incrementTotal();
}
