package com.atlassian.hibernate.extras;

public interface HibernateTranslator
{
    public ExportHibernateHandle objectOrHandleToHandle(Object object);
    public Object objectOrHandleToObject(Object object);
    public ExportHibernateHandle objectToHandle(Object object);
    public Object handleToObject(ExportHibernateHandle handle);
}
