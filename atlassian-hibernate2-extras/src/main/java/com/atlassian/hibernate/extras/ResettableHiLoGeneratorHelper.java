package com.atlassian.hibernate.extras;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.persister.EntityPersister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.orm.hibernate.SessionFactoryUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is used to set the hibernate_unique_key.next_hi value to the high bits of the highest id in the DB, +1
 * after an import to maintain state in the database.
 * <p>
 * A direct copy from version used in Bamboo
 */
@SuppressWarnings("unused")
public class ResettableHiLoGeneratorHelper
{
    private static final Logger log = LoggerFactory.getLogger(ResettableHiLoGeneratorHelper.class);

    private static final String HIBERNATE_UNIQUE_KEY_TABLE = "hibernate_unique_key";
    private static final String HIBERNATE_UNIQUE_KEY_COLUMN = "next_hi";

    private final SessionFactory sessionFactory;

    public ResettableHiLoGeneratorHelper(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setNextHiValue() throws HibernateException, SQLException
    {
        Connection connection = SessionFactoryUtils.getSession(sessionFactory, true).connection();
        try
        {
            Set<ResettableTableHiLoGenerator> generators = new HashSet<>();
            Statement statement = connection.createStatement();
            try
            {
                long maximumId = 0;
                int maxLo = 0;
                for (Object o : sessionFactory.getAllClassMetadata().keySet())
                {
                    Class<?> c = (Class) o;
                    String className = c.toString();
                    ClassPersister persister = ((SessionFactoryImplementor) sessionFactory).getPersister(c);
                    IdentifierGenerator identifierGenerator = persister.getIdentifierGenerator();
                    if (persister instanceof EntityPersister && identifierGenerator instanceof ResettableTableHiLoGenerator)
                    {
                        EntityPersister entityPersister = (EntityPersister) persister;
                        ResettableTableHiLoGenerator generator = (ResettableTableHiLoGenerator) identifierGenerator;
                        generators.add(generator);
                        if (maxLo == 0)
                        {
                            maxLo = generator.getMaxLo();
                        }
                        else if (maxLo != generator.getMaxLo())
                        {
                            throw new IllegalStateException("One generator uses " + maxLo + " for maxLo, generator for " + className + " uses " + generator.getMaxLo());
                        }

                        String[] idColumnNames = entityPersister.getIdentifierColumnNames();
                        if (idColumnNames.length != 1)
                        {
                            throw new IllegalStateException("expected a single id column for " + className + " found " + idColumnNames.length);
                        }
                        ResultSet rs = statement.executeQuery("select max(" + idColumnNames[0] + ") from " + entityPersister.getTableName());
                        try
                        {
                            if (rs.next())
                            {
                                long value = rs.getLong(1); // might be null, but that's ok, will return 0;
                                log.info("Maximum id for " + className + " is " + value);
                                if (value > maximumId)
                                {
                                    maximumId = value;
                                }
                            }
                        }
                        finally
                        {
                            JdbcUtils.closeResultSet(rs);
                        }
                    }
                }
                int nextHi = (int) (maximumId / (maxLo + 1)) + 1;
                log.info("Setting new next_hi to " + nextHi);
                if (statement.executeUpdate("update " + HIBERNATE_UNIQUE_KEY_TABLE + " set " + HIBERNATE_UNIQUE_KEY_COLUMN + " = " + nextHi) == 0)
                {
                    // no row, need to insert one
                    if (statement.executeUpdate("insert into " + HIBERNATE_UNIQUE_KEY_TABLE + " values(" + nextHi + ")") == 0)
                    {
                        throw new IllegalStateException("failed to insert initial next_hi value");
                    }
                }
            }
            finally
            {
                JdbcUtils.closeStatement(statement);
            }
            generators.forEach(ResettableTableHiLoGenerator::reset);
            connection.commit();
        }
        catch (HibernateException | Error | RuntimeException | SQLException exception)
        {
            connection.rollback();
            throw exception;
        }
    }
}
