package com.atlassian.hibernate.extras;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.dialect.Dialect;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerationException;
import net.sf.hibernate.id.TableGenerator;
import net.sf.hibernate.type.Type;
import net.sf.hibernate.util.PropertiesHelper;

/**
 * An implementation of a resettable hi/lo {@code IdentifierGenerator} which is compatible with Hibernate 2.
 * <blockquote>
 * <b>hilo</b><br>
 * <br>
 * An <tt>IdentifierGenerator</tt> that returns a <tt>Long</tt>, constructed using
 * a hi/lo algorithm. The hi value MUST be fetched in a separate transaction
 * to the <tt>Session</tt> transaction so the generator must be able to obtain
 * a new connection and commit it. Hence this implementation may not
 * be used when Hibernate is fetching connections from an application
 * server datasource or when the user is supplying connections. In these
 * cases a <tt>SequenceHiLoGenerator</tt> would be a better choice (where
 * supported).<br>
 * <br>
 * Mapping parameters supported: table, column, max_lo
 * </blockquote>
 */
public class ResettableTableHiLoGenerator extends TableGenerator
{
    /**
     * The max_lo parameter
     */
    public static final String MAX_LO = "max_lo";

    private static final Logger log = LoggerFactory.getLogger(ResettableTableHiLoGenerator.class);

    private long hi;
    private int lo;
    private int maxLo;
    private Class<?> returnClass;

    public void configure(Type type, Properties params, Dialect d)
    {
        super.configure(type, params, d);

        maxLo = PropertiesHelper.getInt(MAX_LO, params, Short.MAX_VALUE);
        lo = maxLo + 1; // so we "clock over" on the first invocation
        returnClass = type.getReturnedClass();
    }

    public synchronized Serializable generate(SessionImplementor session, Object obj) throws SQLException, HibernateException
    {
        if (lo > maxLo)
        {
            long hival = ((Number) super.generate(session, obj)).longValue();
            lo = 1;
            hi = hival * (maxLo + 1);
            log.debug("new hi value: " + hival);
        }

        return createNumber(hi + lo++, returnClass);
    }

    /**
     * Set the lo value to more than maxLo to force a reset of the hi value.
     */
    public synchronized void reset()
    {
        lo = maxLo + 1;
    }

    /**
     * Copied from IdentifierGeneratorFactory, where it is package protected
     */
    private static Number createNumber(long value, Class clazz) throws IdentifierGenerationException
    {
        if (clazz == Long.class)
        {
            return value;
        }
        else if (clazz == Integer.class)
        {
            return (int) value;
        }
        else if (clazz == Short.class)
        {
            return (short) value;
        }
        else
        {
            throw new IdentifierGenerationException("this id generator generates long, integer, short");
        }
    }

    /**
     * Get the max_lo value, so that we can calculate a valid value for the next_hi DB value,
     * given the ids already allocated in the database.
     */
    public int getMaxLo()
    {
        return maxLo;
    }
}
