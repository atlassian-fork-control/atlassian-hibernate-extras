package com.atlassian.hibernate.extras.tangosol;

import net.sf.hibernate.cache.Cache;
import net.sf.hibernate.cache.CacheException;

/**
 * An extended version of the standard Hibernate {@link net.sf.hibernate.cache.Cache} interface that adds
 * extra operations that are required by {@link com.atlassian.hibernate.extras.tangosol.CoherenceCacheStrategy}.
 *
 * This interface should be implemented by the host application's Hibernate cache implementation.
 */
public interface AtlassianHibernateCache extends Cache
{
    boolean containsKey(Object key) throws CacheException;

    <T,K,V> T invoke(K key, CacheEntryProcessor<K,V, T> processor) throws CacheException;
}
