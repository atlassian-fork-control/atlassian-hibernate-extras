package com.atlassian.hibernate.extras;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;
import net.sf.hibernate.Transaction;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestXMLDatabinder {

    @Mock
    private SessionFactoryImplementor sessionFactoryImplementor;

    @Mock
    private HibernateTranslator hibernateTranslator;

    private XMLDatabinder sut;

    @Before
    public void before() throws HibernateException {
        Session session = mock(Session.class);
        when(session.beginTransaction()).thenReturn(mock(Transaction.class));
        when(sessionFactoryImplementor.openSession()).thenReturn(session);

        ClassPersister persister = mock(ClassPersister.class);
        when(persister.getPropertyTypes()).thenReturn(new Type[0]);
        when(persister.getPropertyNames()).thenReturn(new String[0]);
        when(sessionFactoryImplementor.getPersister(Object.class)).thenReturn(persister);

        when(hibernateTranslator.objectOrHandleToHandle(any(ExportHibernateHandle.class))).then(invocation -> invocation.getArguments()[0]);

        sut = new XMLDatabinder(sessionFactoryImplementor, "UTF-8", hibernateTranslator) {
            @Override
            public boolean parseCustomType(Writer writer, Type type, Object value, String xmlValue) {
                return false;
            }
        };
    }

    @Test
    public void exportOfExistentObjectSucceeds() throws IOException, HibernateException, ParserConfigurationException, SAXException {
        // Contains one object that will be found in the database and returned as per the normal case
        List<ExportHibernateHandle> handles = prepareHibernateHandles(
                new Object()
        );

        sut.bindAll(handles);

        try (Writer writer = new StringWriter()) {
            sut.toGenericXML(writer, mock(ExportProgress.class));
            Assert.assertEquals("XML should be valid and there should be one exported child element", 1, getNumberOfExportedEntities(writer.toString()));
        }
    }

    @Test
    public void exportContainingNonExistentObjectCompletes() throws IOException, HibernateException, SAXException, ParserConfigurationException {
        // A chain of handlers with one object that returns null when queried (e.g. deleted from the database in a separate transaction)
        List<ExportHibernateHandle> handles = prepareHibernateHandles(
                new Object(),
                null,
                new Object()
        );

        sut.bindAll(handles);

        try (Writer writer = new StringWriter()) {
            sut.toGenericXML(writer, mock(ExportProgress.class));
            Assert.assertEquals("XML should be valid and only the two valid objects should be exported", 2, getNumberOfExportedEntities(writer.toString()));
        }
    }

    private List<ExportHibernateHandle> prepareHibernateHandles(Object... objects) {
        return Arrays.stream(objects)
                .map(object -> {
                    ExportHibernateHandle handle = mock(ExportHibernateHandle.class);
                    when(handle.getClazz()).thenReturn(Object.class);
                    when(hibernateTranslator.handleToObject(handle)).thenReturn(object);
                    return handle;
                })
                .collect(Collectors.toList());
    }

    private int getNumberOfExportedEntities(String xml) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xml.replace("\n", "")))).getDocumentElement().getChildNodes().getLength();
    }
}
