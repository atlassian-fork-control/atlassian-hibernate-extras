package com.atlassian.hibernate.extras.hql;

import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.hql.internal.ast.HqlSqlWalker;
import org.hibernate.hql.spi.TemporaryTableBulkIdStrategy;
import org.hibernate.persister.entity.Queryable;

/**
 * An extension of Hibernate's {@code TemporaryTableBulkIdStrategy} that uses joins instead of sub-queries in
 * {@code where} clauses to delete against the temporary ID table.
 * <p>
 * To use this class you need to specify it as the implementation of {@code MultiTableBulkIdStrategy} while
 * configuring {@code SettingsFactory} for the Hibernate session about to be built.
 *
 * @since 5.0.0
 * @see JoinTableBasedDeleteHandlerImpl
 */
public class JoinTemporaryTableBulkIdStrategy extends TemporaryTableBulkIdStrategy {

    @Override
    public DeleteHandler buildDeleteHandler(SessionFactoryImplementor factory, HqlSqlWalker walker) {
        return new JoinTableBasedDeleteHandlerImpl(factory, walker) {
            @Override
            protected void prepareForUse(Queryable persister, SessionImplementor session) {
                createTempTable(persister, session);
            }

            @Override
            protected void releaseFromUse(Queryable persister, SessionImplementor session) {
                releaseTempTable(persister, session);
            }
        };
    }
}
