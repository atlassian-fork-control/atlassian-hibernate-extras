package com.atlassian.hibernate.extras.type;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.java.StringTypeDescriptor;

/**
 * Emulates Hibernate's built-in {@code TextType}, but using a {@code CLOB} column in the database rather than using
 * {@code LONGVARCHAR}.
 * <p>
 * There's a bit of history in the necessity for this type:
 * <ul>
 * <li>When Stash was first started, the Liquibase {@code HsqlDatabase} was customised to use {@code CLOB} and
 * {@code BLOB} for LOBs instead of its default {@code LONGVARCHAR} and {@code LONGVARBINARY}
 * <ul>
 * <li>When using Hibernate's <i>default</i> types for LOB columns, this is what is expected</li>
 * <li>When using {@code TextType} (which was introduced in 3.6), it expects the other types instead</li>
 * <li>Because it's a bit late to undo the Liquibase customisations, we need a version of {@code TextType}
 * which expects the columns to be {@code CLOB}s</li>
 * </ul>
 * </li>
 * <li>Postgres CLOBs do not support Unicode characters when using OIDs; instead, the text must be stored directly
 * in the {@code TEXT} column (which the {@code TextType} would do)</li>
 * <li>Oracle's support for {@code LONGVARCHAR} and {@code LONGVARBINARY} are deprecated</li>
 * </ul>
 * This type itself is nothing special. All it does is replace the {@code LongVarcharTypeDescriptor} {@code TextType}
 * would use for the SQL side of the mapping with a new {@link ClobTypeDescriptor}. On the Java side of the mapping,
 * Hibernate's standard {@code StringTypeDescriptor} is used, which ensures the {@code CLOB} is bound to statements
 * using {@code Statement.setString(...)} instead of {@code Statement.setClob(...)}.
 *
 * @see ClobTypeDescriptor
 * @since 4.0
 */
@SuppressWarnings("unused") //Used by Hibernate via Reflection
public class ClobType extends AbstractSingleColumnStandardBasicType<String>
{
    /**
     * Constructs a new {@code ClobType} which uses:
     * <ul>
     * <li>{@link ClobTypeDescriptor#INSTANCE} for its SQL-side mapping</li>
     * <li>{@code StringTypeDescriptor.INSTANCE} for its Java-side mapping</li>
     * </ul>
     */
    public ClobType()
    {
        super(ClobTypeDescriptor.INSTANCE, StringTypeDescriptor.INSTANCE);
    }

    /**
     * Retrieves a simple name for this type.
     *
     * @return {@code "clob"}
     */
    @Override
    public String getName()
    {
        return "clob";
    }
}
