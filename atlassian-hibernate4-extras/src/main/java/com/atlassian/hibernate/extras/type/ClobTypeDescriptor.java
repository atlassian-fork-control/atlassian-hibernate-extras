package com.atlassian.hibernate.extras.type;

import org.hibernate.type.descriptor.sql.LongVarcharTypeDescriptor;

import java.sql.Types;

/**
 * Overrides Hibernate's standard {@code LongVarcharTypeDescriptor} to use {@code CLOB} as its SQL type rather than
 * {@code LONGVARCHAR}. All other aspects of how this class functions are the same as its base class.
 * <p>
 * Under Postgres, the implementation of this type descriptor ensures the <i>text</i> for the {@code CLOB} is stored
 * directly in the {@code TEXT} column, rather than an OID pointing to a large object. This must be done because all
 * versions of the Postgres JDBC driver use {@code Clob.getAsciiStream()} to stream data into the created large object
 * rather than {@code Clob.getCharacterStream()}, meaning any Unicode characters in the text would be lost. When the
 * text is stored in the column, the encoding of the database is used instead and Unicode characters are preserved.
 * <p>
 * Note: For all other RDMBSs, Hibernate's default types for dealing with {@code CLOB} columns would produce the
 * correct results. Postgres support is the sum reason for the existence of this type.
 *
 * @see ClobType
 * @since 4.0
 */
public class ClobTypeDescriptor extends LongVarcharTypeDescriptor
{
    /**
     * A reusable singleton instance for this descriptor, allowing it to be shared amongst different Hibernate
     * {@code Type} instances since it maintains no internal state.
     */
    public static final ClobTypeDescriptor INSTANCE = new ClobTypeDescriptor();

    /**
     * Overrides {@code LongVarcharTypeDescriptor.getSqlType()} to return {@code CLOB} instead of {@code LONGVARCHAR}.
     *
     * @return {@code Types.CLOB}
     */
    @Override
    public int getSqlType()
    {
        return Types.CLOB;
    }
}
