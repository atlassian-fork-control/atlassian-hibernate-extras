package com.atlassian.hibernate.extras.batching;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * A reusable batching helper for Criteria using "in" restrictions, so as not to exceed the limits of various databases.
 * Criteria objects are not easily reusable in hibernate, so a new criteria is created for each batch. Clients supply a
 * {@link Consumer} instance to apply additional criteria per batch (this class handles the "in" restriction).
 * <p>
 * Sample usage:
 * <pre><code>
 * List&lt;String&gt; userNames = new ArrayList&lt;&gt;("user1", "user2");
 * CriteriaBatchedInClauseHelper&lt;Result&gt; resultsForUsers = new CriteriaBatchedInClauseHelper&lt;&gt;(
 *         Result.class, "lowerName", 1000,
 *         new Predicate&lt;Result&gt;() {
 *
 *             &#064;Override
 *             public boolean test(Result result) {
 *                 return true;
 *             }
 *         });
 * List&lt;Result&gt; results = resultsForUsers.executeAndCollect(session, userNames,
 *         criteria -&gt; criteria.add(Restrictions.eq("column", "value")));
 * </code></pre>
 *
 * @param <S> the type used for the Criteria class and predicate.
 */
public class CriteriaBatchedInClauseHelper<S>
{
    private final Class<S> criteriaClass;
    private final String inClauseParameterName;
    private final int batchSize;
    private final Predicate<S> predicate;

    /**
     * @param criteriaClass         the criteria class
     * @param inClauseParameterName the name of the "in" parameter
     * @param batchSize             the size of a single batch executed. Should be at least 1. not be more than 1000 due
     *                              to Oracle's in clause limit - <a href="https://community.oracle.com/thread/958612?tstart=0">ORA-01795</a>
     * @param predicate             Predicate that visits with every rowset returned from the criteria and decides if processing
     *                              should be continued or stopped.
     */
    public CriteriaBatchedInClauseHelper(final Class<S> criteriaClass,
                                         final String inClauseParameterName,
                                         final int batchSize,
                                         final Predicate<S> predicate)
    {
        this.criteriaClass = criteriaClass;
        this.inClauseParameterName = inClauseParameterName;
        this.batchSize = batchSize;
        this.predicate = predicate;
    }

    /**
     * @param session               the Hibernate Session in which the queries will be executed
     * @param inClauseObjects       list of objects for the "in" clause"
     * @param criteriaConsumer      consumer that gets a chance to modify the (new) criteria for each batch
     * @return a list with all processed items
     */
    public <T> List<S> executeAndCollect(final Session session, final Iterable<T> inClauseObjects, final Consumer<Criteria> criteriaConsumer)
    {
        final ImmutableList.Builder<S> allResultsBuilder = ImmutableList.builder();
        // Break into separate criteria to keep the size of the in clause below the configured limit
        for (final List<T> subList : Iterables.partition(inClauseObjects, batchSize))
        {
            final Criteria criteria = session.createCriteria(criteriaClass);
            criteria.add(Restrictions.in(inClauseParameterName, subList));
            criteriaConsumer.accept(criteria);

            //noinspection unchecked
            for (final S object : (List<S>) criteria.list())
            {
                if (predicate.test(object))
                {
                    allResultsBuilder.add(object);
                }
                else
                {
                    return allResultsBuilder.build();
                }
            }
        }
        return allResultsBuilder.build();
    }
}