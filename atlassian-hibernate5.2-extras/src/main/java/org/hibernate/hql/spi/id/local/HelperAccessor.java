package org.hibernate.hql.spi.id.local;

import org.hibernate.boot.TempTableDdlTransactionHandling;
import org.hibernate.engine.spi.SharedSessionContractImplementor;

/**
 * This class exists to provide access to {@link Helper#releaseTempTable}, which is {@code protected} (unlike
 * {@link Helper#createTempTable}, which is {@code public}). This class allows accessing the {@code protected}
 * method without resorting to Reflection. The advantage to this approach is that signature changes will break
 * <i>compilation</i>, whereas Reflection fails at runtime.
 *
 * @since 6.1.0
 */
public class HelperAccessor {

    private HelperAccessor() {
    }

    //Technically this doesn't need to be here. It's here for consistency, so that both methods can be
    //used by calling this accessor
    public static void createTempTable(IdTableInfoImpl idTableInfo,
                                       TempTableDdlTransactionHandling ddlTransactionHandling,
                                       SharedSessionContractImplementor session) {
        Helper.INSTANCE.createTempTable(idTableInfo, ddlTransactionHandling, session);
    }

    public static void releaseTempTable(IdTableInfoImpl idTableInfo, AfterUseAction afterUseAction,
                                        TempTableDdlTransactionHandling ddlTransactionHandling,
                                        SharedSessionContractImplementor session) {
        Helper.INSTANCE.releaseTempTable(idTableInfo, afterUseAction, ddlTransactionHandling, session);
    }
}
